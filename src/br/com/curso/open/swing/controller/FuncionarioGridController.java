/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.curso.open.swing.controller;

import br.com.curso.open.swing.view.FuncionarioView;
import java.util.ArrayList;
import java.util.Map;
import org.openswing.swing.message.receive.java.Response;
import org.openswing.swing.message.receive.java.VOListResponse;
import org.openswing.swing.table.client.GridController;
import org.openswing.swing.table.java.GridDataLocator;

/**
 *
 * @author Leonel
 */
public class FuncionarioGridController extends GridController implements GridDataLocator {

    public FuncionarioView funcionarioView;

    public FuncionarioGridController() {

        funcionarioView = new FuncionarioView();
        funcionarioView.setVisible(true);
    }

    @Override
    public Response loadData(int action,
            int startIndex,
            Map filteredColumns,
            ArrayList currentSortedColumns,
            ArrayList currentSortedVersusColumns,
            Class valueObjectType,
            Map otherGridParams) {

        return new VOListResponse();
    }

    public FuncionarioView getFuncionarioView() {

        return funcionarioView;
    }
}
