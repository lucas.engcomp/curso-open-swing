DROP TABLE IF EXISTS funcionario;
CREATE TABLE public.funcionario (
  id serial,
  nome VARCHAR(255) NOT NULL,
  sexo CHAR(1) NOT NULL,
  salario NUMERIC(15,2),
  data_de_entrada DATE NOT NULL,
  data_de_saida DATE,
  cidade VARCHAR(50) NOT NULL,
  logradouro VARCHAR(255) NOT NULL,
  numero VARCHAR(10),
  complemento VARCHAR(255),
  bairro VARCHAR(50) NOT NULL,
  uf CHAR(2) NOT NULL,
  cep CHAR(8),
  fk_cargo INTEGER NOT NULL,
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS cargo;
CREATE TABLE public.cargo (
  id serial,
  cargo VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);